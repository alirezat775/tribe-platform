package com.app.sample.app.feed.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.feed.domain.model.FeedModel
import com.app.sample.app.base.*
import com.app.sample.app.databinding.ActivitySampleFeedBinding
import com.app.sample.app.feed.viewmodel.SampleFeedViewModel
import com.google.android.material.snackbar.Snackbar

class SampleFeedActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySampleFeedBinding

    private val viewModel: SampleFeedViewModel by lazy {
        ViewModelProviders.of(this, BaseViewModelFactory { SampleFeedViewModel() })
            .get(SampleFeedViewModel::class.java)
    }

    private val feedAdapter = SampleFeedAdapter()

    private var endCursor: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySampleFeedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.init(this)
        setupRecyclerView()
        observeOnData()
        viewModel.getFeedWithLogin()
    }

    private fun setupRecyclerView() {
        binding.recyclerView.apply {
            adapter = feedAdapter
            layoutManager = LinearLayoutManager(this@SampleFeedActivity)
            addOnScrollListener(object :
                EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    viewModel.getFeedPagination(endCursor)
                }
            })
        }
        feedAdapter.actionListener = {
            Toast.makeText(this, it.item.post.title + " Clicked!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun observeOnData() {
        viewModel.feedState.observe(this) {
            when (it) {
                is ViewData -> {
                    setDataOnAdapter(it)
                    binding.loading.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.empty.visibility = View.GONE
                }
                is ViewEmpty -> {
                    binding.loading.visibility = View.GONE
                    binding.recyclerView.visibility = View.GONE
                    binding.empty.visibility = View.VISIBLE
                }
                is ViewError -> {
                    endCursor = null
                    binding.loading.visibility = View.GONE
                    binding.recyclerView.visibility = View.GONE
                    binding.empty.visibility = View.VISIBLE
                    Snackbar.make(binding.root, it.message.toString(), Snackbar.LENGTH_LONG).show()
                }
                is ViewLoading -> {
                    binding.loading.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                    binding.empty.visibility = View.GONE
                }
            }
        }
    }

    private fun setDataOnAdapter(it: ViewData<FeedModel>) {
        endCursor = it.data.endCursor
        feedAdapter.addItems(it.data.postList ?: listOf())
        val lastPosition = feedAdapter.itemCount - 1
        feedAdapter.notifyItemRangeInserted(lastPosition, it.data.postList?.size ?: 0)
    }
}