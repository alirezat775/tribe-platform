package com.app.sample.app

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.sample.app.auth.SampleAuthActivity
import com.app.sample.app.databinding.ActivityMainBinding
import com.app.sample.app.feed.view.SampleFeedActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.auth.setOnClickListener {
            val intent = Intent(this, SampleAuthActivity::class.java)
            startActivity(intent)
        }
        binding.feed.setOnClickListener {
            val intent = Intent(this, SampleFeedActivity::class.java)
            startActivity(intent)
        }
    }
}