package com.app.sample.app.base

sealed class ViewState<out T>
object ViewLoading : ViewState<Nothing>()
data class ViewError(val message: String? = "") : ViewState<Nothing>()
data class ViewEmpty(val message: String? = "") : ViewState<Nothing>()
data class ViewData<T>(val data: T) : ViewState<T>()
