package com.app.sample.app.feed.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.community.CommunityManager
import com.app.feed.domain.model.FeedModel
import com.app.sample.app.base.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SampleFeedViewModel : ViewModel() {

    private val _feedState = MutableLiveData<ViewState<FeedModel>>()
    val feedState: LiveData<ViewState<FeedModel>> = _feedState

    fun init(context: Context) {
        CommunityManager.init(context = context, communityUrl = "community.tribe.so")
    }

    fun getFeedWithLogin() {
        _feedState.value = ViewLoading
        viewModelScope.launch {
            CommunityManager.getInstance().loginGuest({
                getFeedPagination("")
            }, {
                _feedState.value = ViewError(it.messages.toString())
            })
        }
    }

    fun getFeedPagination(cursor: String?) {
        if (cursor == null) {
            return
        }
        viewModelScope.launch(Dispatchers.Main) {
            CommunityManager.getInstance().getFeed(10, cursor, {
                _feedState.value = if (it.postList?.isNullOrEmpty() == false) {
                    ViewData(it)
                } else {
                    ViewEmpty()
                }
            }, {
                _feedState.value = ViewError(it.messages.toString())
            })
        }
    }
}