package com.app.sample.feed

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.app.feed.domain.model.PreviewPostModel
import com.app.sample.feed.databinding.AdapterItemSampleFeedBinding
import com.bumptech.glide.Glide

class SampleFeedAdapter : RecyclerView.Adapter<SampleFeedAdapter.ViewHolder>() {

    private var items: MutableList<PreviewPostModel> = mutableListOf()

    var actionListener: ((OnItemClick) -> Unit)? = null

    fun setItems(items: List<PreviewPostModel>) {
        this.items.clear()
        this.items.addAll(items)
    }

    fun addItems(items: List<PreviewPostModel>) {
        this.items.addAll(items)
    }

    override fun getItemCount() = items.size

    private fun ViewGroup.inflate(@LayoutRes layout: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layout, this, attachToRoot)

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val holder =
            ViewHolder(AdapterItemSampleFeedBinding.bind(viewGroup.inflate(R.layout.adapter_item_sample_feed)))
        holder.actionListener = actionListener
        return holder
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindItems(items[position])
    }

    inner class ViewHolder(private val binding: AdapterItemSampleFeedBinding) :
        RecyclerView.ViewHolder(binding.root) {

        var actionListener: ((OnItemClick) -> Unit)? = null

        @SuppressLint("SetTextI18n")
        fun bindItems(item: PreviewPostModel) {
            binding.title.text = item.post.title

            val htmlContent =
                "<link rel='stylesheet' type='text/css' href='file:///android_asset/style/style.css' />" + item.post.shortContent
            binding.shortContent.loadDataWithBaseURL(
                null, htmlContent, "text/html", "utf-8", null
            )

            Glide.with(binding.root.context)
                .load(item.post.ownerPicture)
                .centerCrop()
                .placeholder(R.drawable.ic_baseline_account_circle_24)
                .into(binding.ownerPicture)

            binding.root.setOnClickListener {
                actionListener?.invoke(OnItemClick(item, adapterPosition))
            }
        }
    }

    class OnItemClick(val item: PreviewPostModel, val position: Int)
}
