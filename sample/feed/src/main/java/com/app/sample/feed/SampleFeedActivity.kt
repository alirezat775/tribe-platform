package com.app.sample.feed

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.community.CommunityManager
import com.app.sample.feed.databinding.ActivitySampleFeedBinding
import kotlinx.coroutines.launch

class SampleFeedActivity : AppCompatActivity() {

    private var endCursor: String? = ""
    private lateinit var binding: ActivitySampleFeedBinding

    private val feedAdapter = SampleFeedAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySampleFeedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        CommunityManager.init(context = this, communityUrl = "community.tribe.so")

        binding.recyclerView.apply {
            adapter = feedAdapter
            layoutManager = LinearLayoutManager(this@SampleFeedActivity)
            addOnScrollListener(object :
                EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    loadFeed(endCursor)
                }
            })
        }
        feedAdapter.actionListener = {
            Toast.makeText(this, it.item.post.title + " Clicked!", Toast.LENGTH_SHORT).show()
        }

        loginGuest()
    }

    private fun loadFeed(cursor: String?) {
        if (cursor == null) {
            return
        }
        lifecycleScope.launch {
            CommunityManager.getInstance().getFeed(10, cursor, {
                endCursor = it.endCursor
                feedAdapter.addItems(it.postList ?: listOf())
                val lastPosition = feedAdapter.itemCount - 1
                feedAdapter.notifyItemRangeInserted(lastPosition, it.postList?.size ?: 0)
            }, {
                endCursor = null
                Toast.makeText(
                    this@SampleFeedActivity, "Error: " + it.messages.toString(), Toast.LENGTH_SHORT
                ).show()
            })
        }
    }

    private fun loginGuest() {
        lifecycleScope.launch {
            CommunityManager.getInstance().loginGuest({
                loadFeed(endCursor)
            }, {
                Toast.makeText(
                    this@SampleFeedActivity, "Error: " + it.messages.toString(), Toast.LENGTH_SHORT
                ).show()
            })
        }
    }

}