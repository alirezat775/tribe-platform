package com.app.sample.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.app.auth.presenter.AuthManager
import com.app.sample.auth.databinding.BottomSheetFragmentAuthBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.launch

class AuthBottomDialogFragment : BottomSheetDialogFragment() {

    private lateinit var binding: BottomSheetFragmentAuthBinding

    companion object {
        val TAG: String? = AuthBottomDialogFragment::class.java.name

        fun newInstance(): AuthBottomDialogFragment {
            return AuthBottomDialogFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = BottomSheetFragmentAuthBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.authLoginButton.setOnClickListener {
            val username = binding.authLoginUsernameInput.text.toString()
            val password = binding.authLoginPasswordInput.text.toString()
            login(username, password)
        }
    }

    private fun login(username: String, password: String) {
        lifecycleScope.launch {
            AuthManager.getInstance().login(username, password, {
                Toast.makeText(requireContext(), "Hi " + it.member?.name, Toast.LENGTH_SHORT).show()
                dismiss()
            }, {
                Toast.makeText(
                    requireContext(), "Error: " + it.messages.toString(), Toast.LENGTH_SHORT
                ).show()
            })
        }
    }
}