package com.app.sample.auth

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.app.auth.presenter.AuthManager
import com.app.sample.auth.databinding.ActivitySampleAuthBinding
import kotlinx.coroutines.launch

class SampleAuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySampleAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySampleAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AuthManager.init(context = this, communityUrl = "community.tribe.so")

        binding.loginGuest.setOnClickListener {
            loginGuest()
        }
        binding.loginUser.setOnClickListener {
            loginUser()
        }
        binding.logoutUser.setOnClickListener {
            logout()
        }
    }

    private fun loginUser() {
        val authBottomDialogFragment = AuthBottomDialogFragment.newInstance()
        authBottomDialogFragment.show(supportFragmentManager, AuthBottomDialogFragment.TAG)
    }

    private fun loginGuest() {
        lifecycleScope.launch {
            AuthManager.getInstance().loginGuest({
                Toast.makeText(
                    this@SampleAuthActivity, "AccessToken: " + it.accessToken, Toast.LENGTH_SHORT
                ).show()
            }, {
                Toast.makeText(
                    this@SampleAuthActivity, "Error: " + it.messages.toString(), Toast.LENGTH_SHORT
                ).show()
            })
        }
    }

    private fun logout() {
        AuthManager.getInstance().logout()
    }
}