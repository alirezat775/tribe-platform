package com.app.community

import android.content.Context
import com.app.auth.domain.model.AuthTokenModel
import com.app.auth.presenter.AuthManager
import com.app.community.manager_interface.AuthInterface
import com.app.community.manager_interface.FeedInterface
import com.app.core.model.ErrorModel
import com.app.feed.domain.model.FeedModel
import com.app.feed.presenter.FeedManager

class CommunityManager private constructor() : AuthInterface, FeedInterface {

    private var communityUrl: String = ""

    companion object {

        private var instance: CommunityManager? = null

        fun init(context: Context, communityUrl: String) {
            AuthManager.init(context, communityUrl)
            FeedManager.init(context, communityUrl)
            getInstance().communityUrl = communityUrl
        }

        fun getInstance(): CommunityManager {
            if (instance == null) {
                synchronized(CommunityManager::class.java) {
                    if (instance == null) {
                        instance = CommunityManager()
                    }
                }
            }
            return instance!!
        }
    }

    override suspend fun <R> loginGuest(
        ifSuccess: (value: AuthTokenModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R {
        return AuthManager.getInstance().loginGuest(ifSuccess, ifError)
    }

    override suspend fun <R> login(
        username: String, password: String,
        ifSuccess: (value: AuthTokenModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R {
        return AuthManager.getInstance().login(username, password, ifSuccess, ifError)
    }

    override fun logout() {
        return AuthManager.getInstance().logout()
    }

    override suspend fun <R> getFeed(
        limit: Int, after: String,
        ifSuccess: (value: FeedModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R {
        return FeedManager.getInstance().getFeed(limit, after, ifSuccess, ifError)
    }
}