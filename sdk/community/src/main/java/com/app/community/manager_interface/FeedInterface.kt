package com.app.community.manager_interface

import com.app.core.model.ErrorModel
import com.app.feed.domain.model.FeedModel

internal interface FeedInterface {

    suspend fun <R> getFeed(
        limit: Int, after: String,
        ifSuccess: (value: FeedModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R
}