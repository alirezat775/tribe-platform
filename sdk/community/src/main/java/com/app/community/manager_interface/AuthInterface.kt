package com.app.community.manager_interface

import com.app.auth.domain.model.AuthTokenModel
import com.app.core.model.ErrorModel

internal interface AuthInterface {

    suspend fun <R> loginGuest(
        ifSuccess: (value: AuthTokenModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R

    suspend fun <R> login(
        username: String, password: String,
        ifSuccess: (value: AuthTokenModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R

    fun logout()
}