package com.app.feed.data.source

import com.app.core.model.ResultModel
import com.app.feed.data.entity.FeedEntity

internal interface FeedRemoteDataSource {

    suspend fun feed(limit: Int, after: String): ResultModel<FeedEntity>

}