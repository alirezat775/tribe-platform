package com.app.feed.domain.model

data class FeedModel(
    val totalCount: Int?,
    val endCursor: String?,
    val postList: List<PreviewPostModel>?
)

data class PreviewPostModel(
    val cursor: String,
    val post: PreviewPostContentModel
)

data class PreviewPostContentModel(
    val id: String,
    val title: String?,
    val shortContent: String?,
    val ownerPicture: String?
)
