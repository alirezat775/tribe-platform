package com.app.feed.domain

import com.app.core.model.ResultModel
import com.app.feed.domain.model.FeedModel

internal interface FeedRepository {

    suspend fun getFeed(limit: Int, after: String): ResultModel<FeedModel>
}