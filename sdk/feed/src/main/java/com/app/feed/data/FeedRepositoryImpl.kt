package com.app.feed.data

import com.app.core.model.ResultModel
import com.app.core.model.map
import com.app.feed.data.source.FeedRemoteDataSource
import com.app.feed.domain.model.FeedModel
import com.app.feed.domain.FeedRepository

internal class FeedRepositoryImpl constructor(
    private val feedRemoteDataSource: FeedRemoteDataSource,
    private val feedDataMapper: FeedDataMapper
) : FeedRepository {

    override suspend fun getFeed(limit: Int, after: String): ResultModel<FeedModel> {
        return feedRemoteDataSource.feed(limit, after).map { feedDataMapper.feedToModel(it) }
    }
}