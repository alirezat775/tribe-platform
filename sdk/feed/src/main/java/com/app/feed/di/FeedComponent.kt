package com.app.feed.di

import android.content.Context
import com.app.core.di.CoreModule
import com.app.feed.presenter.FeedManager
import dagger.BindsInstance
import dagger.Component

@Component(modules = [FeedModule::class, CoreModule::class])
interface FeedComponent {

    fun inject(feedManager: FeedManager)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun androidContext(context: Context): Builder
        fun build(): FeedComponent
    }
}
