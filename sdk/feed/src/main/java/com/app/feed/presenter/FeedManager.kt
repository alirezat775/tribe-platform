package com.app.feed.presenter

import android.content.Context
import com.app.core.model.ErrorModel
import com.app.core.model.execute
import com.app.feed.di.FeedInjector
import com.app.feed.domain.model.FeedModel
import com.app.feed.domain.usecase.FeedRequestModel
import com.app.feed.domain.usecase.GetFeedUseCase
import javax.inject.Inject

class FeedManager private constructor() {

    init {
        FeedInjector.feedComponent.inject(this)
    }

    private var communityUrl: String = ""

    companion object {

        private var instance: FeedManager? = null

        fun init(context: Context, communityUrl: String) {
            FeedInjector.provideFeedComponent(context)
            getInstance().communityUrl = communityUrl
        }

        fun getInstance(): FeedManager {
            if (instance == null) {
                synchronized(FeedManager::class.java) {
                    if (instance == null) {
                        instance = FeedManager()
                    }
                }
            }
            return instance!!
        }
    }

    @Inject
    internal lateinit var getFeedUseCase: GetFeedUseCase

    suspend fun <R> getFeed(
        limit: Int, after: String,
        ifSuccess: (value: FeedModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R {
        return getFeedUseCase.executeAsync(
            FeedRequestModel(limit, after)
        ).execute(ifSuccess, ifError)
    }
}