package com.app.feed.data.source

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.app.core.model.ResultModel
import com.app.core.model.map
import com.app.core.network.mapper
import com.app.feed.FeedQuery
import com.app.feed.data.FeedDataMapper
import com.app.feed.data.entity.FeedEntity

internal class FeedRemoteDataSourceImpl constructor(
    private val apolloClient: ApolloClient,
    private val feedDataMapper: FeedDataMapper
) : FeedRemoteDataSource {

    override suspend fun feed(limit: Int, after: String): ResultModel<FeedEntity> {
        val result =
            apolloClient.query(FeedQuery(limit = limit, after = Optional.presentIfNotNull(after)))
        return mapper(result).map { data -> feedDataMapper.feedToEntity(data) }
    }
}