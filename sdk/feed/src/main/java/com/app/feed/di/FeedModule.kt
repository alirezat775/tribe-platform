package com.app.feed.di

import com.apollographql.apollo3.ApolloClient
import com.app.feed.data.FeedDataMapper
import com.app.feed.data.FeedRepositoryImpl
import com.app.feed.data.source.FeedRemoteDataSource
import com.app.feed.data.source.FeedRemoteDataSourceImpl
import com.app.feed.domain.FeedRepository
import com.app.feed.domain.usecase.GetFeedUseCase
import dagger.Module
import dagger.Provides

@Module
class FeedModule {

    @Provides
    internal fun provideFeedRepository(
        feedRemoteDataSource: FeedRemoteDataSource,
        feedDataMapper: FeedDataMapper
    ): FeedRepository {
        return FeedRepositoryImpl(feedRemoteDataSource, feedDataMapper)
    }

    @Provides
    internal fun provideFeedDataMapper(): FeedDataMapper {
        return FeedDataMapper()
    }

    @Provides
    internal fun provideFeedRemoteDataSource(
        apolloClient: ApolloClient, feedDataMapper: FeedDataMapper
    ): FeedRemoteDataSource {
        return FeedRemoteDataSourceImpl(apolloClient, feedDataMapper)
    }

    @Provides
    internal fun provideGetFeedUseCase(feedRepository: FeedRepository): GetFeedUseCase {
        return GetFeedUseCase(feedRepository)
    }

}
