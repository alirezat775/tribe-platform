package com.app.feed.data

import com.app.feed.FeedQuery
import com.app.feed.data.entity.FeedEntity
import com.app.feed.data.entity.PreviewPostEntity
import com.app.feed.data.entity.PreviewPostContentEntity
import com.app.feed.domain.model.FeedModel
import com.app.feed.domain.model.PreviewPostModel
import com.app.feed.domain.model.PreviewPostContentModel

internal class FeedDataMapper {

    fun feedToEntity(it: FeedQuery.Data?): FeedEntity {
        with(it?.feed) {
            return FeedEntity(
                postList = this?.edges?.map { previewPostToEntity(it) },
                totalCount = this?.totalCount,
                endCursor = this?.pageInfo?.endCursor
            )
        }
    }

    private fun previewPostToEntity(it: FeedQuery.Edge): PreviewPostEntity {
        return PreviewPostEntity(
            cursor = it.cursor,
            PreviewPostContentEntity(
                id = it.node.id,
                title = it.node.title,
                shortContent = it.node.shortContent,
                ownerPicture = it.node.createdBy?.member?.profilePicture?.onImage?.urls?.thumb
            )
        )
    }

    fun feedToModel(it: FeedEntity): FeedModel {
        return FeedModel(
            postList = it.postList?.map { previewPostToModel(it) },
            totalCount = it.totalCount,
            endCursor = it.endCursor
        )
    }

    private fun previewPostToModel(it: PreviewPostEntity): PreviewPostModel {
        return PreviewPostModel(
            cursor = it.cursor,
            PreviewPostContentModel(
                id = it.post.id,
                title = it.post.title,
                shortContent = it.post.shortContent,
                ownerPicture = it.post.ownerPicture
            )
        )
    }
}