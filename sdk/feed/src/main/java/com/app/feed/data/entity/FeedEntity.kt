package com.app.feed.data.entity

internal data class FeedEntity(
    val totalCount: Int?,
    val endCursor: String?,
    val postList: List<PreviewPostEntity>?
)

internal data class PreviewPostEntity(
    val cursor: String,
    val post: PreviewPostContentEntity
)

internal data class PreviewPostContentEntity(
    val id: String,
    val title: String?,
    val shortContent: String?,
    val ownerPicture: String?,
)
