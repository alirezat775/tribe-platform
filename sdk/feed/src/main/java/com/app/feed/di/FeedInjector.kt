package com.app.feed.di

import android.content.Context

object FeedInjector {

    lateinit var feedComponent: FeedComponent

    fun provideFeedComponent(context: Context): FeedComponent {
        feedComponent = DaggerFeedComponent.builder()
            .androidContext(context)
            .build()
        return feedComponent
    }
}