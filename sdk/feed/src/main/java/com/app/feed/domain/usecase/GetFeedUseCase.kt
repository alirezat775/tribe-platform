package com.app.feed.domain.usecase

import com.app.core.model.ResultModel
import com.app.core.usecase.AsyncUseCase
import com.app.feed.domain.FeedRepository
import com.app.feed.domain.model.FeedModel

internal class GetFeedUseCase constructor(
    private val feedRepository: FeedRepository
) : AsyncUseCase<FeedRequestModel, ResultModel<FeedModel>> {

    override suspend fun executeAsync(rq: FeedRequestModel): ResultModel<FeedModel> {
        return feedRepository.getFeed(rq.limit, rq.after)
    }
}

internal data class FeedRequestModel(
    val limit: Int,
    val after: String
)