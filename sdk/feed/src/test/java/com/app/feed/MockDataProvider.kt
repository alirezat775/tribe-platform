package com.app.feed

import com.app.feed.domain.model.FeedModel
import com.app.feed.domain.model.PreviewPostContentModel
import com.app.feed.domain.model.PreviewPostModel

object MockDataProvider {

    fun providePostContentList(): List<PreviewPostModel> {
        return listOf(
            PreviewPostModel(
                "1",
                PreviewPostContentModel(
                    "3mBxvgZOAxgYX6m",
                    "Welcome to tribe",
                    "<p>Tribe platform is here for helping us to build something awesome! ✌️ </p>",
                    "https://tribe-s3-production.imgix.net/t9iC5GsOHok4ZxnNakaa0?w=1000&auto=compress,format&dl"
                )
            ),
            PreviewPostModel(
                "2",
                PreviewPostContentModel(
                    "69Ri03WpDuV4VlO",
                    "Hi",
                    "<p>In this space we are going to talk about new things that we've learned</p><figure data-id=\"aufgJ8TrMdpyeLXl5Nkx9\" data-type=\"image\"><img data-id=\"aufgJ8TrMdpyeLXl5Nkx9\" data-type=\"image\" src=\"https://tribe-s3-production.imgix.net/aufgJ8TrMdpyeLXl5Nkx9?w=1000&amp;auto=compress,format&amp;dl\"></figure><p> </p>",
                    "https://tribe-s3-production.imgix.net/t9iC5GsOHok4ZxnNakaa0?w=1000&auto=compress,format&dl"
                )
            ),
            PreviewPostModel(
                "3",
                PreviewPostContentModel(
                    "69Ri03WpDuV4VlO",
                    "🎉 Hey 🎉",
                    "<p>I'm posting my new experience here,</p><p>If you want getting more, let's follow me 🙂 🎉 </p>",
                    "https://tribe-s3-production.imgix.net/t9iC5GsOHok4ZxnNakaa0?w=1000&auto=compress,format&dl"
                )
            ),
        )
    }

    fun provideFeedModel(): FeedModel {
        return FeedModel(3, "3", providePostContentList())
    }
}