package com.app.feed

import com.app.core.model.ResultModel
import com.app.core.model.execute
import com.app.feed.domain.FeedRepository
import com.app.feed.domain.model.FeedModel
import com.app.feed.domain.usecase.FeedRequestModel
import com.app.feed.domain.usecase.GetFeedUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestCoroutineExceptionHandler
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class GetFeedUseCaseTest {

    private val repository: FeedRepository = mockk()
    private val getFeedUseCase = GetFeedUseCase(repository)

    @Test
    fun `get empty list from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getFeed(10, "")
            } returns ResultModel.Success(FeedModel(0, "", listOf()))
            getFeedUseCase.executeAsync(FeedRequestModel(10, "")).execute({
                assert(it.postList?.isEmpty() ?: true)
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }

    @Test
    fun `get list from repository`() {
        val testHandler = TestCoroutineExceptionHandler()
        val scope = CoroutineScope(StandardTestDispatcher())
        scope.launch(testHandler) {
            coEvery {
                repository.getFeed(10, "")
            } returns ResultModel.Success(MockDataProvider.provideFeedModel())
            getFeedUseCase.executeAsync(FeedRequestModel(10, "")).execute({
                assert(it.postList?.isNotEmpty() ?: true)
            }, {
                assert(false)
            })
        }
        testHandler.cleanupTestCoroutines()
    }
}