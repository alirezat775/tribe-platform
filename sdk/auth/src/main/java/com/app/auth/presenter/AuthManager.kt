package com.app.auth.presenter

import android.content.Context
import com.app.auth.di.AuthInjector
import com.app.auth.domain.model.AuthTokenModel
import com.app.auth.domain.usecase.AuthGuestUseCase
import com.app.auth.domain.usecase.AuthRemoveTokenUseCase
import com.app.auth.domain.usecase.AuthUserRequestModel
import com.app.auth.domain.usecase.AuthUserUseCase
import com.app.core.model.ErrorModel
import com.app.core.model.execute
import javax.inject.Inject

class AuthManager private constructor() {

    init {
        AuthInjector.authComponent.inject(this)
    }

    private var communityUrl: String = ""

    companion object {

        private var instance: AuthManager? = null

        fun init(context: Context, communityUrl: String) {
            AuthInjector.provideAuthComponent(context)
            getInstance().communityUrl = communityUrl
        }

        fun getInstance(): AuthManager {
            if (instance == null) {
                synchronized(AuthManager::class.java) {
                    if (instance == null) {
                        instance = AuthManager()
                    }
                }
            }
            return instance!!
        }
    }

    @Inject
    internal lateinit var authGuestUseCase: AuthGuestUseCase

    @Inject
    internal lateinit var authUserUseCase: AuthUserUseCase

    @Inject
    internal lateinit var authRemoveTokenUseCase: AuthRemoveTokenUseCase

    suspend fun <R> loginGuest(
        ifSuccess: (value: AuthTokenModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R {
        return authGuestUseCase.executeAsync(communityUrl).execute(ifSuccess, ifError)
    }

    suspend fun <R> login(
        username: String, password: String,
        ifSuccess: (value: AuthTokenModel) -> R,
        ifError: (error: ErrorModel) -> R
    ): R {
        return authUserUseCase.executeAsync(
            AuthUserRequestModel(
                communityUrl, username, password
            )
        ).execute(ifSuccess, ifError)
    }

    fun logout() {
        return authRemoveTokenUseCase.executeSync(Unit)
    }
}