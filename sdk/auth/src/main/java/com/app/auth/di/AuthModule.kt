package com.app.auth.di

import com.apollographql.apollo3.ApolloClient
import com.app.auth.data.AuthDataMapper
import com.app.auth.data.AuthRepositoryImpl
import com.app.auth.data.source.local.AuthLocalDataSource
import com.app.auth.data.source.local.AuthLocalDataSourceImpl
import com.app.auth.data.source.remote.AuthRemoteDataSource
import com.app.auth.data.source.remote.AuthRemoteDataSourceImpl
import com.app.auth.domain.AuthRepository
import com.app.auth.domain.usecase.AuthGuestUseCase
import com.app.auth.domain.usecase.AuthRemoveTokenUseCase
import com.app.auth.domain.usecase.AuthUserUseCase
import com.app.core.cache.CacheProvider
import dagger.Module
import dagger.Provides

@Module
class AuthModule {

    @Provides
    internal fun provideAuthRepository(
        authRemoteDataSource: AuthRemoteDataSource,
        authLocalDataSource: AuthLocalDataSource,
        authDataMapper: AuthDataMapper
    ): AuthRepository {
        return AuthRepositoryImpl(authRemoteDataSource, authLocalDataSource, authDataMapper)
    }

    @Provides
    internal fun provideAuthDataMapper(): AuthDataMapper {
        return AuthDataMapper()
    }

    @Provides
    internal fun provideAuthRemoteDataSource(
        apolloClient: ApolloClient, authDataMapper: AuthDataMapper
    ): AuthRemoteDataSource {
        return AuthRemoteDataSourceImpl(apolloClient, authDataMapper)
    }

    @Provides
    internal fun provideAuthLocalDataSource(
        cacheProvider: CacheProvider
    ): AuthLocalDataSource {
        return AuthLocalDataSourceImpl(cacheProvider)
    }

    @Provides
    internal fun provideLoginGuestUseCase(authRepository: AuthRepository): AuthGuestUseCase {
        return AuthGuestUseCase(authRepository)
    }

    @Provides
    internal fun provideLoginUseCase(authRepository: AuthRepository): AuthUserUseCase {
        return AuthUserUseCase(authRepository)
    }

    @Provides
    internal fun provideAuthRemoveTokenUseCase(authRepository: AuthRepository): AuthRemoveTokenUseCase {
        return AuthRemoveTokenUseCase(authRepository)
    }
}
