package com.app.auth.data.source.remote

import com.app.auth.data.entity.AuthTokenEntity
import com.app.core.model.ResultModel

internal interface AuthRemoteDataSource {

    suspend fun authGuest(url: String): ResultModel<AuthTokenEntity>

    suspend fun authUser(username: String, password: String): ResultModel<AuthTokenEntity>
}