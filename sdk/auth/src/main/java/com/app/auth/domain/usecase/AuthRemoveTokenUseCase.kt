package com.app.auth.domain.usecase

import com.app.auth.domain.AuthRepository
import com.app.core.usecase.SyncUseCase

internal class AuthRemoveTokenUseCase constructor(
    private val authRepository: AuthRepository
) : SyncUseCase<Unit, Unit> {

    override fun executeSync(rq: Unit) {
        authRepository.removeGuestToken()
        authRepository.removeToken()
    }
}