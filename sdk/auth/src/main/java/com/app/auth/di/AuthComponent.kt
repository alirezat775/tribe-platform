package com.app.auth.di

import android.content.Context
import com.app.auth.presenter.AuthManager
import com.app.core.di.CoreModule
import dagger.BindsInstance
import dagger.Component

@Component(modules = [AuthModule::class, CoreModule::class])
interface AuthComponent {

    fun inject(authManager: AuthManager)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun androidContext(context: Context): Builder
        fun build(): AuthComponent
    }
}
