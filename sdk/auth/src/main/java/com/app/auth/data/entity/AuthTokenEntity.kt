package com.app.auth.data.entity

internal data class AuthTokenEntity(
    val accessToken: String?,
    val role: AuthRoleTokenEntity?,
    val member: AuthMemberTokenEntity?
)

internal data class AuthRoleTokenEntity(
    val name: String?,
    val scopes: List<String>?
)

internal data class AuthMemberTokenEntity(
    val id: String?,
    val name: String?
)