package com.app.auth.di

import android.content.Context

object AuthInjector {

    lateinit var authComponent: AuthComponent

    fun provideAuthComponent(context: Context): AuthComponent {
        authComponent = DaggerAuthComponent.builder()
            .androidContext(context)
            .build()
        return authComponent
    }
}