package com.app.auth.domain.usecase

import com.app.auth.domain.AuthRepository
import com.app.auth.domain.model.AuthTokenModel
import com.app.core.model.ResultModel
import com.app.core.model.doOnSuccess
import com.app.core.model.execute
import com.app.core.usecase.AsyncUseCase

internal class AuthUserUseCase constructor(
    private val authRepository: AuthRepository
) : AsyncUseCase<AuthUserRequestModel, ResultModel<AuthTokenModel>> {

    override suspend fun executeAsync(rq: AuthUserRequestModel): ResultModel<AuthTokenModel> {
        val result = authRepository.authGuest(rq.communityUrl).execute({ guest ->
            // send request based on community and username, password
            authRepository.saveGuestToken(guest.accessToken)
            val authResult = authRepository.authUser(rq.username.trim(), rq.password.trim())
            authResult.doOnSuccess { user ->
                authRepository.saveToken(user.accessToken)
            }
            return authResult
        }, {
            ResultModel.Error(it)
        })
        return result
    }
}

internal data class AuthUserRequestModel(
    val communityUrl: String,
    val username: String,
    val password: String
)