package com.app.auth.data

import com.app.auth.data.source.local.AuthLocalDataSource
import com.app.auth.data.source.remote.AuthRemoteDataSource
import com.app.auth.domain.AuthRepository
import com.app.auth.domain.model.AuthTokenModel
import com.app.core.model.ResultModel
import com.app.core.model.map

internal class AuthRepositoryImpl constructor(
    private val authRemoteDataSource: AuthRemoteDataSource,
    private val authLocalDataSource: AuthLocalDataSource,
    private val authDataMapper: AuthDataMapper
) : AuthRepository {

    override suspend fun authGuest(url: String): ResultModel<AuthTokenModel> {
        return authRemoteDataSource.authGuest(url).map { authDataMapper.authTokenToModel(it) }
    }

    override suspend fun authUser(username: String, password: String): ResultModel<AuthTokenModel> {
        return authRemoteDataSource.authUser(username, password)
            .map { authDataMapper.authTokenToModel(it) }
    }

    override fun saveGuestToken(accessToken: String?) {
        authLocalDataSource.saveGuestToken(accessToken)
    }

    override fun removeGuestToken() {
        authLocalDataSource.removeGuestToken()
    }

    override fun saveToken(accessToken: String?) {
        authLocalDataSource.saveToken(accessToken)
    }

    override fun removeToken() {
        authLocalDataSource.removeToken()
    }
}