package com.app.auth.domain.model

data class AuthTokenModel(
    val accessToken: String?,
    val role: AuthRoleTokenModel?,
    val member: AuthMemberTokenModel?
)

data class AuthRoleTokenModel(
    val name: String?,
    val scopes: List<String>?
)

data class AuthMemberTokenModel(
    val id: String?,
    val name: String?
)