package com.app.auth.domain

import com.app.auth.domain.model.AuthTokenModel
import com.app.core.model.ResultModel

internal interface AuthRepository {

    suspend fun authGuest(url: String): ResultModel<AuthTokenModel>

    suspend fun authUser(username: String, password: String): ResultModel<AuthTokenModel>

    fun saveToken(accessToken: String?)

    fun removeToken()

    fun saveGuestToken(accessToken: String?)

    fun removeGuestToken()

}