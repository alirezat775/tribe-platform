package com.app.auth.data.source.remote

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.app.auth.AuthGuestQuery
import com.app.auth.AuthUserMutation
import com.app.auth.data.AuthDataMapper
import com.app.auth.data.entity.AuthTokenEntity
import com.app.auth.type.LoginNetworkWithPasswordInput
import com.app.core.model.ResultModel
import com.app.core.model.map
import com.app.core.network.mapper

internal class AuthRemoteDataSourceImpl constructor(
    private val apolloClient: ApolloClient,
    private val authDataMapper: AuthDataMapper
) : AuthRemoteDataSource {

    override suspend fun authGuest(url: String): ResultModel<AuthTokenEntity> {
        val result = apolloClient
            .query(AuthGuestQuery(url = Optional.Present(url)))
        return mapper(result).map { data -> authDataMapper.guestAuthTokenToEntity(data) }
    }

    override suspend fun authUser(
        username: String, password: String
    ): ResultModel<AuthTokenEntity> {
        val result = apolloClient.mutation(
            AuthUserMutation(
                LoginNetworkWithPasswordInput(usernameOrEmail = username, password = password)
            )
        )
        return mapper(result).map { data -> authDataMapper.userAuthTokenToEntity(data) }
    }
}