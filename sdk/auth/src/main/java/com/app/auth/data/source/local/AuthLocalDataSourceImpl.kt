package com.app.auth.data.source.local

import com.app.core.cache.CacheProvider

private const val GUEST_TOKEN = "GUEST_TOKEN"
private const val USER_TOKEN = "USER_TOKEN"

class AuthLocalDataSourceImpl constructor(
    private val cacheProvider: CacheProvider
) : AuthLocalDataSource {

    override fun saveToken(token: String?) {
        cacheProvider.save(USER_TOKEN, token)
    }

    override fun removeToken() {
        cacheProvider.remove(USER_TOKEN)
    }

    override fun saveGuestToken(token: String?) {
        cacheProvider.save(GUEST_TOKEN, token)
    }

    override fun removeGuestToken() {
        cacheProvider.remove(GUEST_TOKEN)
    }
}