package com.app.auth.data

import com.app.auth.AuthGuestQuery
import com.app.auth.AuthUserMutation
import com.app.auth.data.entity.AuthMemberTokenEntity
import com.app.auth.data.entity.AuthRoleTokenEntity
import com.app.auth.data.entity.AuthTokenEntity
import com.app.auth.domain.model.AuthMemberTokenModel
import com.app.auth.domain.model.AuthRoleTokenModel
import com.app.auth.domain.model.AuthTokenModel

internal class AuthDataMapper {

    fun authTokenToModel(it: AuthTokenEntity): AuthTokenModel {
        return AuthTokenModel(
            accessToken = it.accessToken,
            role = AuthRoleTokenModel(it.role?.name, it.role?.scopes),
            member = AuthMemberTokenModel(it.member?.id, it.member?.name)
        )
    }

    fun guestAuthTokenToEntity(data: AuthGuestQuery.Data?): AuthTokenEntity {
        with(data?.tokens) {
            return AuthTokenEntity(
                accessToken = this?.accessToken,
                role = AuthRoleTokenEntity(this?.role?.name, this?.role?.scopes),
                member = AuthMemberTokenEntity(this?.member?.id, this?.member?.name)
            )
        }
    }

    fun userAuthTokenToEntity(data: AuthUserMutation.Data?): AuthTokenEntity {
        with(data?.loginNetwork) {
            return AuthTokenEntity(
                accessToken = this?.accessToken,
                role = AuthRoleTokenEntity(this?.role?.name, this?.role?.scopes),
                member = AuthMemberTokenEntity(this?.member?.id, this?.member?.name)
            )
        }
    }
}