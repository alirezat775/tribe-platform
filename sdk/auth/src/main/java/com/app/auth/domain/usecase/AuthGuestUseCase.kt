package com.app.auth.domain.usecase

import com.app.auth.domain.AuthRepository
import com.app.auth.domain.model.AuthTokenModel
import com.app.core.model.ResultModel
import com.app.core.model.doOnSuccess
import com.app.core.usecase.AsyncUseCase

internal class AuthGuestUseCase constructor(
    private val authRepository: AuthRepository
) : AsyncUseCase<String, ResultModel<AuthTokenModel>> {

    override suspend fun executeAsync(rq: String): ResultModel<AuthTokenModel> {
        val authResult = authRepository.authGuest(rq)
        authResult.doOnSuccess {
            authRepository.saveToken(it.accessToken)
        }
        return authResult
    }
}