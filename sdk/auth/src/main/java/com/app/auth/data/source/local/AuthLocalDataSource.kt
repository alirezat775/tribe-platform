package com.app.auth.data.source.local

internal interface AuthLocalDataSource {

    fun saveToken(token: String?)
    fun removeToken()

    fun saveGuestToken(token: String?)
    fun removeGuestToken()
}