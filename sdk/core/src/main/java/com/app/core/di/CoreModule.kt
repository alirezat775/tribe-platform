package com.app.core.di

import android.content.Context
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.okHttpClient
import com.app.core.BuildConfig
import com.app.core.cache.CacheProvider
import com.app.core.network.AuthorizationInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

@Module
class CoreModule {

    @Provides
    fun provideApolloClient(okHttpClient: OkHttpClient): ApolloClient {
        return ApolloClient.Builder()
            .httpServerUrl(BuildConfig.API)
            .okHttpClient(okHttpClient)
            .build()
    }

    @Provides
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        authorizationInterceptor: AuthorizationInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .addInterceptor(loggingInterceptor)
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    fun provideCacheProvider(context: Context): CacheProvider {
        return CacheProvider(context = context, encryptType = CacheProvider.Encrypt.DESEDE_ENCRYPT, encryptKey = BuildConfig.ENCRYPT_KEY)
    }

    @Provides
    fun provideAuthorizationInterceptor(cacheProvider: CacheProvider): AuthorizationInterceptor {
        return AuthorizationInterceptor(cacheProvider)
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        return logging
    }
}
