package com.app.core.cache

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.app.core.helper.JsonHelper

internal class SharedPreferencesManager(private val context: Context) : ICacheProvider {

    private val postFixTimeToLife = "timeToLife"

    private companion object {
        private val preferences: String? = null

        @Volatile
        private var sharedPreferences: SharedPreferences? = null

        fun getSharedPreferences(context: Context): SharedPreferences {
            return sharedPreferences ?: synchronized(this) {
                context.getSharedPreferences(preferences, Activity.MODE_PRIVATE)
                    .also { sharedPreferences = it }
            }
        }
    }

    override fun save(key: String, value: String?, timeToLife: Long) {
        addTimeToLife(key, timeToLife)
        getSharedPreferences(context).edit().putString(key, value).apply()
    }

    override fun save(key: String, value: Boolean, timeToLife: Long) {
        addTimeToLife(key, timeToLife)
        getSharedPreferences(context).edit().putBoolean(key, value).apply()
    }

    override fun save(key: String, value: Float, timeToLife: Long) {
        addTimeToLife(key, timeToLife)
        getSharedPreferences(context).edit().putFloat(key, value).apply()
    }

    override fun save(key: String, value: Int, timeToLife: Long) {
        addTimeToLife(key, timeToLife)
        getSharedPreferences(context).edit().putInt(key, value).apply()
    }

    override fun save(key: String, value: Long, timeToLife: Long) {
        addTimeToLife(key, timeToLife)
        getSharedPreferences(context).edit().putLong(key, value).apply()
    }

    override fun save(key: String, value: Any?, timeToLife: Long) {
        addTimeToLife(key, timeToLife)
        val json = JsonHelper.toJson(value!!)
        getSharedPreferences(context).edit().putString(key, json).apply()
    }

    override fun get(key: String, defaultValue: String): String? {
        if (!valid(key)) {
            remove(key + postFixTimeToLife)
            remove(key)
            return defaultValue
        }
        return getSharedPreferences(context).getString(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Boolean): Boolean {
        if (!valid(key)) {
            remove(key + postFixTimeToLife)
            remove(key)
            return defaultValue
        }
        return getSharedPreferences(context).getBoolean(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Float): Float {
        if (!valid(key)) {
            remove(key + postFixTimeToLife)
            remove(key)
            return defaultValue
        }
        return getSharedPreferences(context).getFloat(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Int): Int {
        if (!valid(key)) {
            remove(key + postFixTimeToLife)
            remove(key)
            return defaultValue
        }
        return getSharedPreferences(context).getInt(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Long): Long {
        if (!valid(key)) {
            remove(key + postFixTimeToLife)
            remove(key)
            return defaultValue
        }
        return getSharedPreferences(context).getLong(key, defaultValue)
    }

    override fun get(key: String, defaultValue: String, type: Any): Any? {
        if (!valid(key)) {
            remove(key + postFixTimeToLife)
            remove(key)
            return defaultValue
        }
        val json = getSharedPreferences(context).getString(key, defaultValue)
        return JsonHelper.fromJson(json!!, type as Class<*>)
    }

    override fun remove(key: String) {
        getSharedPreferences(context).edit().remove(key).apply()
    }

    override fun clear() {
        getSharedPreferences(context).edit().clear().apply()
    }

    override fun has(key: String): Boolean {
        return getSharedPreferences(context).contains(key)
    }

    override fun registerChangeListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener) {
        getSharedPreferences(context).registerOnSharedPreferenceChangeListener(changeListener)
    }

    override fun unRegisterChangeListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener) {
        getSharedPreferences(context).unregisterOnSharedPreferenceChangeListener(changeListener)
    }

    override fun valid(key: String): Boolean {
        if (!has(key)) return false

        return when (val timeToLife =
            getSharedPreferences(context).getLong(
                key + postFixTimeToLife,
                CacheProvider.EXPIRE_TIME
            )) {
            CacheProvider.EXPIRE_TIME -> false
            CacheProvider.NONE_EXPIRE_TIME -> true
            else -> System.currentTimeMillis() < timeToLife
        }
    }

    private fun addTimeToLife(key: String, timeToLife: Long) {
        val time = if (timeToLife == CacheProvider.NONE_EXPIRE_TIME)
            timeToLife
        else
            System.currentTimeMillis() + timeToLife

        getSharedPreferences(context).edit()
            .putLong(key + postFixTimeToLife, time)
            .apply()
    }
}