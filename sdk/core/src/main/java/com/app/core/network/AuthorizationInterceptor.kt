package com.app.core.network

import com.app.core.cache.CacheProvider
import okhttp3.Interceptor
import okhttp3.Response

private const val GUEST_TOKEN = "GUEST_TOKEN"
private const val USER_TOKEN = "USER_TOKEN"

class AuthorizationInterceptor(private val cacheProvider: CacheProvider) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val guestToken = cacheProvider.get(GUEST_TOKEN, "")
        val userToken = cacheProvider.get(USER_TOKEN, "")
        val token = if (userToken.isNullOrEmpty()) guestToken else userToken
        val request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()

        return chain.proceed(request)
    }
}