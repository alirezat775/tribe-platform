package com.app.core.helper

import com.google.gson.Gson

object JsonHelper {

    fun toJson(jsonObject: Any): String {
        return Gson().toJson(jsonObject)
    }

    fun <T> fromJson(jsonString: String, classType: Class<T>): T {
        return Gson().fromJson(jsonString, classType)
    }
}