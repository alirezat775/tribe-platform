package com.app.core.usecase

interface AsyncUseCase<RQ, RS> {

    suspend fun executeAsync(rq: RQ): RS

}
