package com.app.core.network

import com.apollographql.apollo3.ApolloCall
import com.apollographql.apollo3.api.Operation
import com.app.core.model.ErrorModel
import com.app.core.model.ResultModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun <D : Operation.Data> mapper(result: ApolloCall<D>): ResultModel<D?> {
    return withContext(Dispatchers.IO) {
        val response = result.execute()
        when {
            response.data != null -> {
                return@withContext ResultModel.Success(response.data)
            }
            response.hasErrors() -> {
                return@withContext ResultModel.Error(ErrorModel(
                    messages = response.errors?.map { err -> err.message }
                ))
            }
            else -> {
                return@withContext ResultModel.Error(ErrorModel(
                    messages = response.errors?.map { err -> err.message }
                ))
            }
        }
    }
}