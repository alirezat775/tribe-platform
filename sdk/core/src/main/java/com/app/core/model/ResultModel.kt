package com.app.core.model

sealed class ResultModel<out V> {

    data class Success<V>(val value: V) : ResultModel<V>()

    data class Error(val error: ErrorModel) : ResultModel<Nothing>()
}

data class ErrorModel(
    val messages: List<String>? = null,
    val extensions: Map<String, Any?>? = null,
)

fun <R> ResultModel<R>.doOnSuccess(ifSuccess: (value: R) -> Unit) {
    if (this is ResultModel.Success) {
        ifSuccess.invoke(this.value)
    }
}

fun <R> ResultModel<R>.doOnError(ifError: (value: ErrorModel) -> Unit) {
    if (this is ResultModel.Error) {
        ifError.invoke(this.error)
    }
}

fun <T, R> ResultModel<T>.map(isSuccess: (T) -> R): ResultModel<R> {
    return when (this) {
        is ResultModel.Success -> ResultModel.Success(isSuccess.invoke(value))
        is ResultModel.Error -> ResultModel.Error(error)
    }
}

inline fun <R, T> ResultModel<T>.execute(
    ifSuccess: (value: T) -> R,
    ifError: (error: ErrorModel) -> R
): R {
    return when (this) {
        is ResultModel.Success<T> -> ifSuccess(value)
        is ResultModel.Error -> ifError(ErrorModel(error.messages, error.extensions))
    }
}