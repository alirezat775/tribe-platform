package com.app.core.cache

import androidx.annotation.StringDef

@StringDef(CacheProvider.SHARED_PREFERENCES_MANAGER)
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class CacheType