package com.app.core.cache

import androidx.annotation.LongDef

@LongDef(
    CacheProvider.NONE_EXPIRE_TIME,
    CacheProvider.ONE_SECOND,
    CacheProvider.ONE_MINUTES,
    CacheProvider.ONE_HOURS,
    CacheProvider.ONE_DAYS,
    CacheProvider.ONE_WEEKS,
    CacheProvider.ONE_MONTH,
    CacheProvider.ONE_YEAR
)
@kotlin.annotation.Retention(AnnotationRetention.SOURCE)
annotation class TimeType