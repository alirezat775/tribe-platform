package com.app.core.cache

import android.content.Context
import android.content.SharedPreferences
import com.app.core.helper.CryptHelper
import java.security.KeyException

internal interface ICacheProvider {

    fun save(
        key: String,
        value: String?,
        @TimeType timeToLife: Long = CacheProvider.NONE_EXPIRE_TIME
    )

    fun save(
        key: String,
        value: Boolean,
        @TimeType timeToLife: Long = CacheProvider.NONE_EXPIRE_TIME
    )

    fun save(key: String, value: Float, @TimeType timeToLife: Long = CacheProvider.NONE_EXPIRE_TIME)
    fun save(key: String, value: Int, @TimeType timeToLife: Long = CacheProvider.NONE_EXPIRE_TIME)
    fun save(key: String, value: Long, @TimeType timeToLife: Long = CacheProvider.NONE_EXPIRE_TIME)
    fun save(key: String, value: Any?, @TimeType timeToLife: Long = CacheProvider.NONE_EXPIRE_TIME)

    fun get(key: String, defaultValue: String): String?
    fun get(key: String, defaultValue: Boolean): Boolean
    fun get(key: String, defaultValue: Float): Float
    fun get(key: String, defaultValue: Int): Int
    fun get(key: String, defaultValue: Long): Long
    fun get(key: String, defaultValue: String, type: Any): Any?

    fun remove(key: String)
    fun clear()

    fun registerChangeListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener)
    fun unRegisterChangeListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener)

    fun has(key: String): Boolean
    fun valid(key: String): Boolean
}

class CacheProvider(
    context: Context,
    @CacheType type: String = SHARED_PREFERENCES_MANAGER,
    private val encryptType: Encrypt = Encrypt.NONE_ENCRYPT,
    encryptKey: String = "SECRET_KEY_MUST_BE_24_CH"
) : ICacheProvider {

    enum class Encrypt {
        NONE_ENCRYPT,
        DESEDE_ENCRYPT
    }

    companion object {

        const val SHARED_PREFERENCES_MANAGER: String = "SharedPreferencesManager"

        internal const val NONE_EXPIRE_TIME = 0L
        internal const val EXPIRE_TIME = -1L

        const val ONE_SECOND = 1000L
        const val ONE_MINUTES = 60 * ONE_SECOND
        const val ONE_HOURS = 60 * ONE_MINUTES
        const val ONE_DAYS = 24 * ONE_HOURS
        const val ONE_WEEKS = 7 * ONE_DAYS
        const val ONE_MONTH = 30 * ONE_DAYS
        const val ONE_YEAR = 365 * ONE_DAYS
    }

    private lateinit var kesho: ICacheProvider

    init {
        when (type) {
            SHARED_PREFERENCES_MANAGER -> kesho = SharedPreferencesManager(context)
        }
        if (encryptType == Encrypt.DESEDE_ENCRYPT) {
            if (encryptKey == "SECRET_KEY_MUST_BE_24_CH" || encryptKey.length < 23) {
                throw KeyException("please set valid key with 24 character")
            }
            CryptHelper.setKey(encryptKey)
        }
    }

    override fun save(key: String, value: String?, @TimeType timeToLife: Long) {
        var mValue = value
        if (encryptType == Encrypt.DESEDE_ENCRYPT) {
            if (value == null)
                throw NullPointerException("value not be null when using encryption method")
            mValue = CryptHelper.encrypt(value)
        }
        kesho.save(key, mValue, timeToLife)
    }

    override fun save(key: String, value: Boolean, @TimeType timeToLife: Long) {
        kesho.save(key, value, timeToLife)
    }

    override fun save(key: String, value: Float, @TimeType timeToLife: Long) {
        kesho.save(key, value, timeToLife)
    }

    override fun save(key: String, value: Int, @TimeType timeToLife: Long) {
        kesho.save(key, value, timeToLife)
    }

    override fun save(key: String, value: Long, @TimeType timeToLife: Long) {
        kesho.save(key, value, timeToLife)
    }

    override fun save(key: String, value: Any?, @TimeType timeToLife: Long) {
        kesho.save(key, value, timeToLife)
    }

    override fun get(key: String, defaultValue: String): String? {
        val value: String? = kesho.get(key, defaultValue)
        if (encryptType == Encrypt.DESEDE_ENCRYPT) {
            return CryptHelper.decrypt(value!!)
        }
        return value
    }

    override fun get(key: String, defaultValue: Boolean): Boolean {
        return kesho.get(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Float): Float {
        return kesho.get(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Int): Int {
        return kesho.get(key, defaultValue)
    }

    override fun get(key: String, defaultValue: Long): Long {
        return kesho.get(key, defaultValue)
    }

    override fun get(key: String, defaultValue: String, type: Any): Any? {
        return kesho.get(key, defaultValue, type)
    }

    override fun remove(key: String) {
        kesho.remove(key)
    }

    override fun clear() {
        kesho.clear()
    }

    override fun registerChangeListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener) {
        kesho.registerChangeListener(changeListener)
    }

    override fun unRegisterChangeListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener) {
        kesho.unRegisterChangeListener(changeListener)
    }

    override fun has(key: String): Boolean {
        return kesho.has(key)
    }

    override fun valid(key: String): Boolean {
        return kesho.valid(key)
    }
}