package com.app.core.usecase

interface SyncUseCase<RQ, RS> {

    fun executeSync(rq: RQ): RS

}
